﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MedIndexTest.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MedIndexTest.Tests.Logic
{
    [TestClass]
    public class StorageTest
    {
        [TestMethod]
        public void GetKeysTest()
        {
            var values = new Dictionary<string, object>();
            values.Add("key1", "value1");
            values.Add("key2", "value2");
            values.Add("key3", "value3");

            var storage = new Storage(values);
            var keys = storage.GetKeys();
            Assert.AreEqual(3, keys.Count);
            Assert.IsTrue(keys.Contains("key1"));
            Assert.IsTrue(keys.Contains("key2"));
            Assert.IsTrue(keys.Contains("key3"));
            Assert.IsFalse(keys.Contains("not existing key"));
        }

        [TestMethod]
        public void GetValueTest()
        {
            var storage = new Storage();
            storage.Add("key1", "value1");
            var value = storage.Get("key1");
            Assert.AreEqual("value1", value);
        }

        [TestMethod]
        public void AddValueTest()
        {
            var storage = new Storage();
            storage.Add("key1", "value1");
            var keys = storage.GetKeys();
            Assert.IsTrue(keys.Contains("key1"));
            var value = storage.Get("key1");
            Assert.AreEqual("value1", value);
        }

        [TestMethod]
        public void UpdateValueTest()
        {
            var storage = new Storage();
            storage.Add("key1", "value1");
            storage.Add("key1", "value2");
            var value = storage.Get("key1");
            Assert.AreEqual("value2", value);
        }

        [TestMethod]
        public void DeleteValueTest()
        {
            var storage = new Storage();
            storage.Add("key1", "value1");
            var keys = storage.GetKeys();
            Assert.IsTrue(keys.Contains("key1"));
            storage.Delete("key1");
            keys = storage.GetKeys();
            Assert.IsFalse(keys.Contains("key1"));
        }

        [TestMethod]
        public void ConcurrencyTest()
        {
            var storage = new Storage();
            List<Task> tasks = new List<Task>();
            for (var i = 0; i < 100; i++)
            {
                var t = new Task(() => storage.Add("key", "value"));
                t.Start();
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
            var keys = storage.GetKeys();
            var value = storage.Get("key");
            Assert.AreEqual(1, keys.Count);
            Assert.AreEqual("value", value);
        }
    }
}
