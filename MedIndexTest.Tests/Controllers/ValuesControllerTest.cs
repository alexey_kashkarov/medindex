﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MedIndexTest;
using MedIndexTest.Controllers;
using MedIndexTest.Logic;

namespace MedIndexTest.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        private ValuesController ctrl;
        private IStorage<object> storage;

        [TestInitialize]
        public void Init()
        {
            storage = new Storage();
            ctrl = new ValuesController(storage);
        }

        [TestMethod]
        public void Get()
        {
            storage.Add("key1", "value1");
            storage.Add("key2", "value2");
            storage.Add("key3", "value3");

            // Act
            IEnumerable<string> result = ctrl.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("key3", result.ElementAt(0));
            Assert.AreEqual("key2", result.ElementAt(1));
            Assert.AreEqual("key1", result.ElementAt(2));
        }

        [TestMethod]
        public void GetById()
        {
            storage.Add("key1", "value1");

            // Act
            string result = ctrl.Get("key1");

            // Assert
            Assert.AreEqual("value1", result);
        }

        [TestMethod]
        public void Post()
        {
            // Act
            ctrl.Post("key1", "value1");

            // Assert
            var keys = storage.GetKeys();
            var value = storage.Get("key1");
            Assert.IsTrue(keys.Contains("key1"));
            Assert.AreEqual("value1", value);
        }

        [TestMethod]
        public void Put()
        {
            // Act
            ctrl.Put("key1", "value1");

            // Assert
            var keys = storage.GetKeys();
            var value = storage.Get("key1");
            Assert.IsTrue(keys.Contains("key1"));
            Assert.AreEqual("value1", value);
        }

        [TestMethod]
        public void Delete()
        {
            // Arrange
            ctrl.Put("key1", "value1");

            var keys = storage.GetKeys();
            Assert.IsTrue(keys.Contains("key1"));

            // Act
            ctrl.Delete("key1");

            // Assert
            keys = storage.GetKeys();
            Assert.IsFalse(keys.Contains("key1"));
        }
    }
}
