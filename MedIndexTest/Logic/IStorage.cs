﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedIndexTest.Logic
{
    public interface IStorage<T>
    {
        void Add(string key, T value);
        object Get(string key);
        void Delete(string key);
        ICollection<string> GetKeys();
    }
}