﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedIndexTest.Logic
{
    public class Storage : IStorage<object>
    {
        private readonly ConcurrentDictionary<string, object> storage;
        private object _lockObj = new object();

        public Storage()
        {
            this.storage = new ConcurrentDictionary<string, object>();
        }

        public Storage(ICollection<KeyValuePair<string, object>> values)
        {
            this.storage = new ConcurrentDictionary<string, object>(values);
        }

        public void Add(string key, object value)
        {
            if (storage.TryGetValue(key, out object obj))
            {
                lock (_lockObj)
                {
                    storage[key] = value;
                }
            }
            else
            {
                storage.TryAdd(key, value);
            }
        }

        public void Delete(string key)
        {
            storage.TryRemove(key, out object obj);
        }

        public object Get(string key)
        {
            return storage[key];
        }

        public ICollection<string> GetKeys()
        {
            return storage.Keys;
        }
    }
}