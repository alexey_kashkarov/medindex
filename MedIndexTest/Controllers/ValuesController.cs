﻿using MedIndexTest.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MedIndexTest.Controllers
{
    public class ValuesController : ApiController
    {

        private IStorage<object> storage;

        public ValuesController(IStorage<object> storage)
        {
            this.storage = storage;
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return storage.GetKeys();
        }

        // GET api/values/5
        public string Get(string key)
        {
            return storage.Get(key).ToString();
        }

        // POST api/values
        public void Post(string key, [FromBody]string value)
        {
            storage.Add(key, value);
        }

        // PUT api/values/5
        public void Put(string key, [FromBody]string value)
        {
            storage.Add(key, value);
        }

        // DELETE api/values/5
        public void Delete(string key)
        {
            storage.Delete(key);
        }
    }
}
