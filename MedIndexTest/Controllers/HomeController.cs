﻿using MedIndexTest.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedIndexTest.Controllers
{
    public class HomeController : Controller
    {
        private IStorage<object> storage;

        public HomeController(IStorage<object> storage)
        {
            this.storage = storage;
        }

        public ActionResult Index()
        {
            var model = storage.GetKeys();
            return View(model);
        }

        public ActionResult Get(string id)
        {
            return Json(storage.Get(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Set()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Set(string key, string value)
        {
            storage.Add(key, value);
            return RedirectToAction("Index");
        }

        public ActionResult Remove()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Remove(string key)
        {
            storage.Delete(key);
            return RedirectToAction("Index");
        }
    }
}
